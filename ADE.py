# -*-coding:Utf-8 -*
## This is a Python module written to retrieve information from an ADE server ##
# Author: Matthieu Guerquin-Kern, 2022

import requests # HTTP(S) connections
from bs4 import BeautifulSoup # XML parsing
import pandas as pd # (>= 1.5) deal with complex data structures, import and export
import re # for regex
from datetime import timedelta

TREE_PATH = '/jsp/standard/gui/tree.jsp'
SCHEDULE_PATH = '/jsp/custom/modules/plannings/direct_planning.jsp'
EV4RES_PATH = '/jsp/custom/modules/plannings/info.jsp'
RES_INFO_PATH = '/jsp/custom/modules/infos/infos.jsp'
EV_INFO_PATH = '/jsp/custom/modules/plannings/eventInfo.jsp'
PIANO_WEEKS_PATH = '/jsp/custom/modules/plannings/pianoWeeks.jsp'

def ListToCSV( l: list ) -> list:
	"""Make a string that separates the items of a list with commas.
	"""
	return ','.join([str(e) for e in l])
	
def GetIDsInHtmlTable(soup, colIndex:int, match:str, pre:str, post:str) -> list:
	"""When ADE displays info on an event (EV_INFO_PATH), in the form of a table,
	   the event IDs are to be found in a javascript HREF links.
	   This functions parses the HTML table to return the list of event IDs.
	"""
	return [[int(idx) if idx.isdigit() else None for idx in re.findall(f"{pre}([^{post}]*){post}",str(row))[0].split(',')] for row in soup.select(f'td:nth-of-type({colIndex+1}) > a[href*="{match}"]')]

def CountHours( df: pd.core.frame.DataFrame ) -> float:
	"""In a pandas DataFrame that contains a column 'Duration', sum up the
	   hours whatever the type of activity.
	"""
	return df['Duration'].sum()
	
def CountHETD( df: pd.core.frame.DataFrame ) -> float:
	"""In a pandas DataFrame that contains columns 'Duration' and 'Type',
	   sum up the hours of teaching activities (CM, CTD, CTD2, TD, TP, DSTP, BE) with
	   a particular weight of 3/2 when the Type is 'CM' and 2 when the Type is
	   'CTD2'.
	   This corresponds to "Heures Équivalent TD" for tenured teachers at Grenoble INP.
	"""
	if df is None:
		return 0
	else:
		dfCM = df[df.Type == 'CM']
		dfCTD = df[df.Type == 'CTD']
		dfCTD2 = df[df.Type == 'CTD2']
		dfTD = df[df.Type == 'TD']
		dfTP = df[df.Type == 'TP']
		dfDSTP = df[df.Type == 'DSTP']
		dfBE = df[df.Type == 'BE']
		#dfMeeting = df[df.Type == 'Réunion']
		#dfFreeWork = df[df.Type == 'Travail en salle']
		return 2*dfCTD2['Duration'].sum()+1.5*dfCM['Duration'].sum()+dfCTD['Duration'].sum()+dfTD['Duration'].sum()+dfTP['Duration'].sum()+dfDSTP['Duration'].sum()+dfBE['Duration'].sum()

class session():
	def __init__(self, server, prefix='', login=None, pwd=None, ev_db=None, res_db=None, verbose=False):
		"""The init routine of the class. Gets executed when instantiating.
		"""
		self.server = server # protocol+server name (string)
		self.prefix = prefix # URL path prefix (string)
		self.login  = login  # username on server (string)
		self.ev_db  = ev_db  # database of collected events (pandas DataFrame)
		self.res_db = res_db # database of collected resources (pandas DataFrame)
		self.session = requests.Session() # create session
		if pwd is not None:
			self.session.auth = (self.login, pwd) # for safety, the password does not persist after init
		if verbose:
			print(f'-> Initiating connection as "{login}" on "{server}"')
		result = self.session.get(self.server) # first contact
		result.raise_for_status() # check for response code
		if len(re.findall(r'Unauthorized',str(result.content))): # when authentication goes wrong
			raise TypeError('-> Cannot access server: wrong credentials?')
		self.__WeekIndexes() # collect info to build the week dict
		if self.res_db == None:
			self.res_db = pd.DataFrame(columns = ['resID','name','code','type', 'web', 'email', 'qty', 'capacity', 'category']).astype({'resID':'int','name':'str','code':'str','type':'str','web':'str','email':'str','qty':'int','capacity':'int','category':'str'})
			self.res_db.set_index('resID',inplace = True)
		if self.ev_db == None:
			self.ev_db = pd.DataFrame(columns = ['evID','Time','Activity Name','Code','Type','Rank','Siblings','Duration','Teachers','Rooms','Student Group'])
			self.ev_db.set_index('evID',inplace = True)
			
	def __get(self, path):
		"""Internal routine for HTML GET requests
		""" 
		result = self.session.get(self.server+self.prefix+path)
		result.raise_for_status()
		return result.content
		
	def __post(self, path, data):
		"""Internal routine for HTML POST requests
		""" 
		result = self.session.post(self.server+self.prefix+path, data=data)
		result.raise_for_status()
		return result.content
		
	def FindResource(self, search_str:str, verbose=False) -> dict:
		"""Find the resources on ADE corresponding to a particular search string.
		"""
		if verbose:
			print(f'-> Looking for "{search_str}" in ADE''s tree...')
		xml = self.__post(TREE_PATH, {'search': search_str})
		soup = BeautifulSoup(xml, 'html.parser')
		targets = soup.select('span[class="treeitem"] > a')
		Dict = {}
		for a in targets:
			Dict[a.string] = int(re.findall(r'\b\d+\b', a.get("href"))[0]) # Dict['name'] = ID
		return Dict

	def ResNameToID(self, name:str, cat:str=None) -> int:
		"""Returns the resource ID matching the provided
		   resource name. By default, the match must be exact.
		   If the optional argument 'cat' is provided,
		   the match can be partial and items are filtered to fit the category.
		   If several resources are eligible, the ID of the first one is
		   returned.
		"""
		if cat is None: # seeking an exact name match
			ids = self.res_db.index[self.res_db.name == name]
		else: # seek a partial case insensitive match that fits the category
			sel = self.res_db[self.res_db.category == str(cat)]['name']
			ids = sel.index[sel.str.contains(name,case=False,regex=False)]
		if len(ids)==0:# interrogate the server
			ids = [int(i) for i in self.FindResource(name).values()]
			if cat is not None:
				for i in ids:
					self.ResourceInfo(i)
				sel = self.res_db[self.res_db.index.isin(ids)]
				ids = sel[sel.category == str(cat)].index # select category
		if len(ids) == 0:
			return []
		else:
			return ids[0]# there should be one and only one
			
	def isFree(self, resID, evID) -> pd.core.frame.DataFrame:
		"""Returns a MultiIndex of Booleans:
			- Row indexes are resource IDs and names,
			- column indexes are events IDs, start time and stop time.
		"""
		if isinstance(resID, int):
			resID = [resID]
		if isinstance(evID, int):
			evID = [evID]
		BoolArray = [None]*len(evID)
		evStart = [None]*len(evID)
		evStop = [None]*len(evID)
		for i,e in enumerate(evID):
			edf = self.EventInfo(e)
			evStart[i] = edf['Time']
			evStop[i] = edf['Time']+timedelta(hours=edf['Duration'])	
			evWeek = self.weekdict[edf['Time'].isocalendar().week]
			evDay = edf['Time'].weekday()
			BoolList = [True]*len(resID)
			for j,r in enumerate(resID):
				self.ResourceInfo(r) # just to get the resource Name
				df = self.EventsForResIDs(resID[j], days=[evDay],weeks=[evWeek])
				if df is not None:
					for k in range(len(df)):
						start = df.iloc[k].Time
						stop = df.iloc[k].Time+timedelta(hours=df.iloc[k].Duration)
						if (evStart[i]<=stop) and (evStop[i]>=start):
							BoolList[j] = False
							break
				BoolArray[i] = BoolList
		indexCols = pd.MultiIndex.from_arrays([evID,evStart,evStop], names=('evID', 'Start', 'Stop'))
		resNames = [self.res_db.loc[r]['name'] for r in resID]
		indexRows = pd.MultiIndex.from_arrays([resID,resNames], names=('resID', 'Name'))
		return pd.DataFrame(BoolArray,index=indexCols, columns=indexRows).transpose()
	
	def __WeekIndexes(self):
		"""Internal routine building the dictionary that translates an ISO
		   calendar week number into the corresponding ADE week index.
		"""
		xml = self.__get(PIANO_WEEKS_PATH)
		soup = BeautifulSoup(xml, 'html.parser')
		t1 = soup.select('body > div[class="pianoline"] > div > img')
		t2 = soup.select('body > div[class="pianoline"] > div > map > area')
		weekindexes = [int(re.sub(r'^javascript:push\((\d+),[\s\S]*',r'\1',t.get("href"))) for t in t2[0::2]]
		weeknames = [t.get("alt") for t in t1]
		weeknumbers = [int(re.sub(r'^s(\d+)  [\s\S]*',r'\1',n)) for n in weeknames]# calendar year
		weeknames = [re.sub(r'^s\d+  ([\s\S]*)$',r'\1',n) for n in weeknames]
		self.weekdict = dict(zip(weeknumbers, weekindexes))
		
	def EventsForResIDs(self, resources, days=range(0,5),weeks=range(1,40), verbose=False):
		"""Find events on ADE for a list of resource IDs (list of integers).
		   Optionally, a particular time slot can be specified:
		   	-> days: list of integers (0 means Monday, 7 means Sunday)
			-> weeks: list of integers (week indexes, see self.weekdict)
		"""
		daylist = ListToCSV(days)
		weeklist = ListToCSV(weeks)
		if isinstance(resources,list): resources = ListToCSV(resources)
		if verbose:
			print(f'-> Looking for events related to resource "{resources}" on the ADE server...')
		self.__get(SCHEDULE_PATH+f'?weeks={weeklist}&days={daylist}&resources={resources}')
		xml = self.__get(EV4RES_PATH)
		soup = BeautifulSoup(xml, 'html.parser')
		eventIdList = [e[0] for e in GetIDsInHtmlTable(soup,1,'javascript:ev',"\\(","\\)")]
		if len(eventIdList)>0:# there is at least one event
			df = pd.DataFrame(columns = ['evID','Time','Activity Name','Code','Type','Rank','Siblings','Duration','Teachers','Rooms','Student Group'])
			df.set_index('evID',inplace = True) # event Ids are indexes
			for ev in eventIdList:
				df_list = [df, self.EventInfo(ev).to_frame().T]
				df = pd.concat([d for d in df_list if not d.empty], axis=0, ignore_index=False, sort=False)# DataFrame + Series
			df = df.sort_values(by='Time') # sort by chronological order
			return df
		else:
			if verbose:
				print('no event for the selected period')
			return None
			
	def ResourceInfo(self, resourceID):
		if resourceID not in self.res_db.index: # search only for unknown resources
			xml = self.__get(RES_INFO_PATH+f'?uniqueId={resourceID}')
			soup = BeautifulSoup(xml, 'html.parser')
			#name = soup.find('span',class_="title").text
			data = soup.select('div[class="content"] > div > p')
			if len(data)>7: del data[-1]
			data = [d.text.strip('\n') for d in data]
			data = [int(d) if d.isdigit() else d for d in data]
			#Dict = {'ID':resourceID,'name':data[0],'code':data[1],'type':data[2],'web':data[3],'email':data[4],'qty':data[5],'capacity':data[6],
			#	'category':soup.select_one('input[name="category"]').get('value')}
			#ser = pd.Series(data=Dict)
			#data.insert(0,resourceID)
			data.append(soup.select_one('input[name="category"]').get('value'))
			self.res_db.loc[resourceID] = data
		return self.res_db.loc[resourceID] # Actually returns a Series
	
	def EventInfo(self, eventID):
		if eventID not in self.ev_db.index: # search only for unknown events
		# Name, Type, Siblings, rank in group
		#print(f'-> Looking for info on event "{eventID}" on the ADE server...')
			xml = self.__get(EV_INFO_PATH+f'?eventId={eventID}')
			soup = BeautifulSoup(xml, 'html.parser')
			name = soup.select_one('div.header').string
			target = soup('label')
			code = re.sub(r'Code : ',r'',target[0].string)
			Type = re.sub(r'Type : ',r'',target[1].string)
			eventIdList = [e[0] for e in GetIDsInHtmlTable(soup,1,'javascript:ev',"\\(","\\)")]
			teacherIdList = GetIDsInHtmlTable(soup,7,'javascript:info',"'","'")
			roomIdList = GetIDsInHtmlTable(soup,8,'javascript:info',"'","'")
			#groupIdList = GetIDsInHtmlTable(soup,12,'javascript:info(')
			df = pd.read_html(xml,extract_links=None,skiprows=2)
			df = df[0]# There should be only one tabular on this page...
			df = df.drop(columns=[2,3,6,9,10,11,13,14,15])# get rid of uninteresting columns
			# indexes are:  0   ,	   1	   ,  4   ,	 5	,	7	 ,  8   ,   12
			df.columns = ['Date','Activity Name','Time','Duration','Teachers','Rooms','Student Group']
			# Make list of resources involved
			df['Teachers'] = [[t if t is None else self.ResourceInfo(t)['name'] for t in tL] for tL in teacherIdList]
			df['Rooms'] = [[r if r is None else self.ResourceInfo(r)['name'] for r in rL] for rL in roomIdList]
			#df['Group'] = [[g if g is None else self.ResourceInfo(g)['name'] for g in gL] for gL in groupIdList]
			df['Date'] = pd.to_datetime(df['Date'],format='%d/%m/%Y', errors='coerce')
			df['Time'] = df['Time'].replace(to_replace=r'^(\d+)h(\d+)$', value=r'\1:\2:00', regex=True)
			df['Time'] = pd.to_timedelta(df['Time'])
			# merge Date and Starting time
			df['Date'] += df['Time']
			df['Duration'] = pd.to_timedelta(df['Duration'].replace([r'^(\d+)h$',r'^(\d+)h(\d{2})min$'],[r'\1:00:00',r'\1:\2:00'],regex=True))/pd.Timedelta(hours=1)
			df = df.drop(columns='Time')
			df=df.rename(columns = {'Date':'Time'})
			df['evID'] = eventIdList
			df.set_index('evID',inplace = True) # event Ids are indexes
			df = df.sort_values(by='Time') # sort by chronological order
			eventIdList = list(df.index)
			# extra info obtained on event:
			df.insert(loc=2, column='Code', value=[code for e in eventIdList])
			df.insert(loc=3, column='Type', value=[Type for e in eventIdList])
			df.insert(loc=4, column='Rank', value=[i for i in range(1,len(eventIdList)+1)])
			df.insert(loc=5, column='Siblings', value=[[ev for ev in eventIdList if ev != e] for e in eventIdList])
			df_list = [self.ev_db, df]
			self.ev_db = pd.concat([d for d in df_list if not d.empty], axis=0, ignore_index=False, sort=False) # merge two DataFrames
		return self.ev_db.loc[eventID] # Actually returns a Series
