# ADE

Python module for collecting information from an ADE server and formatting it as Pandas Series and DataFrames

## Contents

This folder contains a Python module, [ADE.py](ADE.py), and a [Jupyter demo notebook](demo.ipynb).

## Origin

These sources are hosted on a server using hosting [GITLAB](https://gitlab.com).

You can see the activity of the project at
<https://gricad-gitlab.univ-grenoble-alpes.fr/guerquim/ade>.
There, among other things, you can download the latest version of the project files.
You can also access the project through GIT.
```bash
$ git clone https://gricad-gitlab.univ-grenoble-alpes.fr/guerquim/ade.git
```
The project is currently public.

## Requirements

See the [requirements.txt](requirements.txt) file.

Install requirements on a Debian-like OS within a virtual environment:
```bash
$ sudo apt install python3-pip python3-venv
$ python3 -m venv env_ade
$ source env_ade/bin/activate
$ pip3 install -r requirements.txt
```

Install requirements on a Arch-like OS within a virtual environment:
```bash
$ sudo pacman -S python3
$ python3 -m venv env_ade
$ source env_ade/bin/activate
$ pip3 install -r requirements.txt
```

## Usage

Open and play with the [Jupyter demo notebook](demo.ipynb).

This demo notebook looks for a configuration file at `~/.config/ade/ade.ini`, with the following structure:
```
[system]
base_url = https://your.ade.instance.org
path_prefix_url = /eventual/prefix

[user]
username = LOGIN
password = PWD
```

## Authors

Matthieu Guerquin-Kern, 2022-2025

## License
This work is distributed under the terms of the Unlicense.

This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <https://unlicense.org>
